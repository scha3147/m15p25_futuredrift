##############################
##  Author: Shengjie Lu     ##
##  Code Review: Li Wang    ##
##############################

from Tkinter import *
import Tkinter as tk

from selectCategories import SelectCategories
from dataAPI import *
import format
''' THE FIRST PAGE -- Select reference product
    INPUT: All product type and name from DB
    OUTPUT: Selected product
'''


class StartPage(Frame):
    def __init__(self, parent, root):
        #label of our feature drift tool
        Frame.__init__(self, parent)
        label = Label(
            self,
            text="Feature Drift Tool",
            bg="Blue",
            width=20,
            height=2,
            font=format.HEADER_FONT,
            fg="White")
        label.pack(pady=40, padx=50)

        self.database = DataAPI()

        #get all the products we store in the database
        self.options = self.database.getAllProducts()

        #write different to the interface
        self.categoryVar = StringVar()
        self.categoryVar.set("Product Category")

        self.var = StringVar(self)
        self.var.set("Select product type & model")

        #show the product category
        self.option = OptionMenu(
            self, self.var, *self.options, command=self.displayCategory)
        self.option.configure(width=30, bg="White")
        self.option.pack(pady=20)

        self.category = Label(
            self,
            textvariable=self.categoryVar,
            fg="Grey",
            width=50,
            borderwidth=1,
            relief="sunken")
        self.category.pack(pady=20)

        self.upperThreshold = StringVar()
        self.lowerThreshold = StringVar()

        self.selectUppoer = Label(
            self, text="Select Upper Threshold (0.0-1.0)").pack()
        self.upperEntry = Entry(self, textvariable=self.upperThreshold)
        self.upperEntry.insert(0, "0.8")
        self.upperEntry.pack()

        self.selectUppoer = Label(
            self, text="Select Lower Threshold (0.0-1.0)").pack()
        self.lowerEntry = Entry(self, textvariable=self.lowerThreshold)
        self.lowerEntry.insert(0, "0.2")
        self.lowerEntry.pack()

        #next button
        next_step = Button(
            self,
            text="Next",
            bg="Orange",
            width=6,
            font=format.NEXT_FONT,
            fg="White",
            command=lambda: self.getFrame(parent, root).tkraise())
        next_step.pack(pady=20)

    #get to next page
    def getFrame(self, parent, root):
        frame = SelectCategories(parent, root)
        root.upper = float(self.upperThreshold.get())
        root.lower = float(self.lowerThreshold.get())

        print(root.upper)
        print(root.lower)

        if self.var == "Select product type & model":
            tkMessageBox.showinfo(
                title="Reminder",
                message="Please select a model for all products")
        else:
            root.selected.append(
                self.database.getProductIdFromName(self.var.get()))
            self.database.close()

        frame.grid(row=0, column=0, sticky="nsew")
        return frame

    # display the category of certain product the user select
    # return the whole category of the product
    def displayCategory(self, value):
        catePath = ""

        for category in self.database.getProductCategory(self.var.get()):
            catePath = catePath + " >> " + str(category)
        self.categoryVar.set(catePath)

    def testDisplay():
        test_result = ""
        for category in self.database.getProductCategory(self.var.get()):
            test_result = str(category)

        return test_result
