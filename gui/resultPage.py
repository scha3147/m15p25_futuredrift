from Tkinter import *
import Tkinter as tk

from dataAPI import DataAPI
from math import sqrt

import format
''' THE FIFTH PAGE -- Result shown
    INPUT: 
    OUTPUT: Result features
'''


class ResultPage(Frame):
    def __init__(self, parent, root):
        database = DataAPI()
        referenceProduct = database.getNameFromProductId(root.selected[0])

        Frame.__init__(self, parent)
        
        related, nonRelated, similar = self.calSimilarity(root)

        relatedLabel = Label(
            self,
            text=
            "Of the products you were interested in, the following are \nrelated to the %s: " % referenceProduct,
            font=format.LABEL_FONT)
        relatedLabel.grid(row = 1, columnspan=15, pady=10, padx=20, sticky=W)

        count = 0
        for r in related:
            name = database.getNameFromProductId(r[0])
            relatedOutput = Label(
                self,
                text= name + " -- " + str(r[1]),
                font=format.SUBHEADER_FONT,
                fg = "blue")
            relatedOutput.grid(row = 3 + count, columnspan=15, padx=20)
            count += 1
        if not related:
            relatedOutput = Label(
                self,
                text= "None",
                font=format.SUBHEADER_FONT,
                fg = "blue")
            relatedOutput.grid(row = 3 + count, columnspan=15, padx=20)
            count += 1

        # relatedOutput = Listbox(self)
        # relatedOutput.config(width=0)
        # for r in related:
        #     name = database.getNameFromProductId(r)
        #     relatedOutput.insert(END, name)
        # relatedOutput.grid(row = 2)

        similarLabel = Label(
            self,
            text=
            "These products were too similar: ",
            font=format.LABEL_FONT)
        similarLabel.grid(row = 3 + count, columnspan=15, pady=10, padx=20, sticky=W)
        count += 1

        for s in similar:
            name = database.getNameFromProductId(s[0])
            similarOutput = Label(
                self,
                text= name + " -- " + str(s[1]),
                font=format.SUBHEADER_FONT,
                fg = "blue")
            similarOutput.grid(row = 3 + count, columnspan=15, padx=20)
            count += 1
        
        if not similar:
            similarOutput = Label(
                self,
                text= "None",
                font=format.SUBHEADER_FONT,
                fg = "blue")
            similarOutput.grid(row = 3 + count, columnspan=15, padx=20)
            count += 1


        # similarOutput = Listbox(self)
        # for s in similar:
        #     name = database.getNameFromProductId(s)
        #     similarOutput.insert(END, name)
        # similarOutput.grid(row = 2 + len(related) + 1)

        nonRelatedLabel = Label(
            self,
            text=
            "These products were too unrelated: ",
            font=format.LABEL_FONT)
        nonRelatedLabel.grid(row = 3 + count, columnspan=15, padx=20, sticky=W)
        count += 1

        for n in nonRelated:
            name = database.getNameFromProductId(n[0])
            nonRelatedOutput = Label(
                self,
                text= name + " -- " + str(n[1]),
                font=format.SUBHEADER_FONT,
                fg = "blue")
            nonRelatedOutput.grid(row = 3 + count, columnspan=15, padx=20)
            count += 1
        
        if not nonRelated:
            nonRelatedOutput = Label(
                self,
                text= "None",
                font=format.SUBHEADER_FONT,
                fg = "blue")
            nonRelatedOutput.grid(row = 3 + count, columnspan=15, padx=20)
            count += 1

        # nonRelatedOutput = Listbox(self)
        # for n in nonRelated:
        #     name = database.getNameFromProductId(n)
        #     nonRelatedOutput.insert(END, name)
        # nonRelatedOutput.grid(row = 2 + len(related) + 1 + len(similar) + 1)

        database.close()

        button = Button(
            self,
            text="Accept",
            bg="Orange",
            width=8,
            font=format.NEXT_FONT,
            fg="White",
            command=lambda: root.destroy()).grid(row = 3 + count, columnspan=15, pady=10, padx=20)

    def calSimilarity(self, root):
        related = []
        non_related = []
        similar = []

        for i in range(1, len(root.selected)):
            '''apply algorithm to each model - calculate similarity'''
            B = []
            AB = 0
            BB = 0

            for j in range(len(root.wordVectors[0])):
                words = None
                for rule in root.ontology:
                    if root.wordVectors[0][j] in rule:
                        words = list(rule)

                if words == None : words = [root.wordVectors[0][j]]

                value = 0
                for word in words:
                    if word in root.wordVectors[i]:
                        value = 1
                        break

                B.append(value)

            for b in B:
                AB += 1 * b
                BB += b * b

            similarity = AB/(sqrt(len(root.wordVectors[0])) * sqrt(BB))
        
            '''similarity between 0.2~0.8, related product
            similarity lower than 0.2, non-related product - interesting product
            similarity higher than 0.8, similar product'''
            if similarity >= root.lower and similarity <= root.upper:
                related.append((root.selected[i], similarity))
            elif similarity < root.lower:
                non_related.append((root.selected[i], similarity))
            elif similarity > root.upper:
                similar.append((root.selected[i], similarity))

        return related, non_related, similar
