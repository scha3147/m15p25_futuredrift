from Tkinter import *
import Tkinter as tk

from dataAPI import *
from selectModels import SelectModels

import format
import ttk
import ctypes
import tkMessageBox
''' THE THIRD PAGE -- Select the category(s) interested in analysing
    INPUT: Select categories from previous page;
           The 4th level categories (product) under selected categories from DB
    OUTPUT: Selected product(s)

    Research materials you might need:
    1. create-a-tree-view-with-checkboxes -- https://stackoverflow.com/questions/5104330/how-to-create-a-tree-view-with-checkboxes-in-python
    2. dynamically-add-checkboxes-with-tkinter -- https://stackoverflow.com/questions/13862535/dynamically-add-checkboxes-with-tkinter-in-python-2-7

    Reminder: Change the layout method will cause display errors. The layout method for all widgets in one page should be the same.
    For example, do not use grid() for some widgets and pack() for others.
'''
visited = []


class treenode():
    def __init__(self, text):
        self.children = {}
        self.data = text
        self.number = None

    def add(self, list):
        if len(list) > 2:
            if list[0] not in visited:
                visited.append(list[0])
                node = treenode(list[0])
                self.children[list[0]] = node
            else:
                node = self.children[list[0]]
            list.remove(list[0])
            return node.add(list)
        else:
            visited.append(list[0])
            leaf = treenode(list[0])
            self.children[list[0]] = leaf
            leaf.number = list[1]
            return 0

    def getchildren(self):
        for item in self.children:
            print(item)


class SelectProducts(Frame):
    def __init__(self, parent, root, categories=set()):
        self.root = root
        Frame.__init__(self, parent)
        label = Label(
            self,
            text="Select up to 10 product(s) you are interested in analysing",
            font=format.LABEL_FONT)
        label.grid(row=1, columnspan=3, pady=10, padx=20, sticky=W)

        label.pack()
        header = ['number of product']

        # Categorys = [
        #     "Animals & Pet Supplies", "Apparel & Accessories",
        #     "Arts & Entertainment", "Baby & Toddler", "Business & Industrial",
        #     "Cameras & Optics", "Electronics", "Food, Beverages & Tobacco",
        #     "Furniture", "Hardware", "Health & Beauty", "Home & Garden",
        #     "Luggage & Bags", "Mature", "Media", "Office Supplies",
        #     "Religious & Ceremonial", "Software", "Sporting Goods",
        #     "Toys & Games", "Vehicles & Parts"
        # ]

        database = DataAPI()
        temp = database.getCategoriesList()
        database.close()
        rootnode = treenode("root")

        for t in temp:
            if t[0] in categories:
                l = list(t)
                rootnode.add(l)

        self.tree = ttk.Treeview(self, columns=header)
        self.tree.pack(fill=tk.BOTH, expand=1, side=tk.TOP, padx=50)
        self.tree.heading(header[0], text=header[0], anchor=tk.NW)
        self.elements = []
        self.insert(rootnode)

        confirm = Button(
            self,
            text="confirm your selection",
            bg="Orange",
            width=17,
            font=format.NEXT_FONT,
            fg="White",
            command=self.confirmSelection)
        confirm.pack(padx=80, pady=10, side = LEFT)

        self.next_step = Button(
            self,
            text="Next",
            bg="Grey",
            width=6,
            font=format.NEXT_FONT,
            fg="White",
            command=lambda: self.getFrame(parent, root, self.getSelected()))
        self.next_step.config(state=DISABLED)
        self.next_step.pack(side=LEFT, pady=10)

    def insert(self, rootnode):
        for key in rootnode.children:
            self.tree.insert("", "end", key, text=key)
            node1 = rootnode.children[key]
            for key2 in node1.children:
                if node1.children[key2].number is None:
                    self.tree.insert(key, "end", key + key2, text=key2)
                else:
                    self.tree.insert(
                        key,
                        "end",
                        key + key2,
                        text=key2,
                        value=node1.children[key2].number)
                node2 = node1.children[key2]
                for key3 in node2.children:
                    if node2.children[key3].number is None:
                        self.tree.insert(
                            key + key2, "end", key + key2 + key3, text=key3)
                    else:
                        self.tree.insert(
                            key + key2,
                            "end",
                            key + key2 + key3,
                            text=key3,
                            value=node2.children[key3].number)
                    node3 = node2.children[key3]
                    for key4 in node3.children:
                        self.tree.insert(
                            key + key2 + key3,
                            "end",
                            key + key2 + key3 + key4,
                            text=key4,
                            value=node3.children[key4].number)

        return

    def getFrame(self, parent, root, selected):
        frame = SelectModels(parent, root, selected)

        frame.grid(row=0, column=0, sticky="nsew")
        return frame

    def confirmSelection(self):
        if len(self.tree.selection()) > 5:
            tkMessageBox.showinfo(
                title="Reminder",
                message=
                "only up to 5 products are allowed ,please select again!")
        elif self.getAvailable() == 0:
            tkMessageBox.showinfo(
                title="Reminder",
                message=
                "There are no product models in the database for this selection!")
        else:
            # print self.getSelected()
            # self.next_step.grid(rowspan=7, column=1, pady=10, sticky=S)
            # self.next_step.pack(pady=10, side = LEFT)
            self.next_step.config(state = NORMAL, bg="Orange")

    def getSelected(self):
        selected = []

        for product in self.tree.selection():
            item_text = self.tree.item(product, "text")
            selected.append(item_text)

        return selected

    def getAvailable(self):
        total = 0

        for product in self.tree.selection():
            total += int(self.tree.item(product, "value")[0])

        return total
