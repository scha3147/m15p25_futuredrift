from Tkinter import *
import Tkinter as tk

from resultPage import ResultPage
import format

from dataAPI import DataAPI
from wordcloud import WordCloud, STOPWORDS
from operator import itemgetter

#################################
## Author: Edmond Buzby        ##
## Code Reviewer: Wenchang Liu ##
#################################

class AddTerms(Frame):
    def __init__(self, parent, root, indices, length):
        # Initialise frame data
        Frame.__init__(self, parent)
        label = Label(
            self,
            text=
            "Please select the terms which don't represent the product's features or capabilities.",
            font=format.LABEL_FONT)
        label.grid(row=1, columnspan=3, pady=10, padx=20, sticky=W)

        # Read current product's name, manual and stopwords
        database = DataAPI()
        index = indices[0]
        productId = root.selected[index]
        name = database.getNameFromProductId(productId)
        category = database.getProductCategory(name)[-1]
        manual = database.getManualFromProductId(productId).lower()
        removeWords = database.getStopwords(productId) | set(root.wordVectors[index])
        database.close()

        # Display current product name
        name_label = Label(self, text=name, font=format.SUBHEADER_FONT)
        name_label.grid(row=2, columnspan=3, pady=10, padx=20, sticky=W)

        # Display product category
        category_label = Label(self, text=category, font=format.LABEL_FONT)
        category_label.grid(row=3, columnspan=3, pady=10, padx=20, sticky=W)

        # Generate a list of frequent words
        wc = WordCloud(stopwords=removeWords | STOPWORDS)
        words = wc.process_text(manual)
        frequencies = sorted(words.items(), key=itemgetter(1), reverse=True)

        # Display 20 most frequent terms
        row_n = 4
        column_n = 0
        checked = []
        for i in range(20):
            checked.append(IntVar())
        for i in range(20):
            Checkbutton(
                self,
                text=frequencies[i][0],
                variable=checked[i],
                onvalue=1,
                offvalue=0).grid(
                    row=row_n, column=column_n, padx=10, sticky=W)
            if row_n == 10:
                column_n += 1
                row_n = 4
            else:
                row_n += 1

        # Display button to continue to the next page
        next_step = Button(
            self,
            text="Next",
            bg="Orange",
            width=6,
            font=format.NEXT_FONT,
            fg="White",
            command=lambda: self.getFrame(parent, root, removeWords, indices, length, checked, frequencies).tkraise())
        next_step.grid(row=19, column=3, sticky=S)

    # Adds the terms selected by the user to the product's stopwords, adds remaing words to word vector and generates the next page
    def getFrame(self, parent, root, stopwords, indices, length, checked, frequencies):
        # Adding terms to the database
        removeWords, wordvector = self.getRemoved(checked, frequencies)
        totalRemoved = stopwords | removeWords
        database = DataAPI()
        index = indices[0]
        database.updateStopwords(root.selected[index], totalRemoved)
        database.close()

        # Add remaining terms to the word vectors
        addWords = wordvector[:(length - len(root.wordVectors[index]))]
        root.wordVectors[index] += addWords

        # Generate next frame 
        frame = None

        if len(indices) == 1:
            frame = ResultPage(parent, root)  
        elif len(root.wordVectors[index]) < length:
            frame = AddTerms(parent, root, indices, length)
        else:
            frame = AddTerms(parent, root, indices[1:], length)

        frame.grid(row=0, column=0, sticky="nsew")
        return frame

    def getRemoved(self, checked, frequencies):
        removeWords = set()
        wordvector = []

        for i in range(20):
            if checked[i].get() == 1:
                removeWords.add(frequencies[i][0])
            else:
                wordvector.append(frequencies[i][0])

        return removeWords, wordvector