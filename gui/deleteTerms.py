from Tkinter import *
import Tkinter as tk

from resultPage import ResultPage
from addTerms import AddTerms
import format

from dataAPI import DataAPI
from wordcloud import WordCloud, STOPWORDS
from operator import itemgetter
from PIL import ImageTk, Image
''' THE FIFTH PAGE -- Select the terms which don't represent the products features or capabilities
                   -- This page should be shown as many times as the number of selected models
    INPUT: The most 50 frequent words from word cloud
    OUTPUT: Non-selected terms

    Research materials you might need:
    1. dynamically-add-checkboxes-with-tkinter -- https://stackoverflow.com/questions/13862535/dynamically-add-checkboxes-with-tkinter-in-python-2-7

    Reminder: Change the layout method will cause display errors. The layout method for all widgets in one page should be the same.
    For example, do not use grid() for some widgets and pack() for others.
'''

#################################
## Author: Edmond Buzby        ##
## Code reviewer: Wenchang Liu ##
#################################

class DeleteTerms(Frame):
    def __init__(self, parent, root, products, index=0):
        # Initialise frame data
        Frame.__init__(self, parent)
        label = Label(
            self,
            text=
            "Please select the terms which don't represent the product's features or capabilities.",
            font=format.LABEL_FONT)
        label.grid(row=1, columnspan=3, pady=10, padx=20, sticky=W)

        # Read current product's name, manual and stopwords
        database = DataAPI()
        productId = products[index]
        name = database.getNameFromProductId(productId)
        category = database.getProductCategory(name)[-1]
        manual = database.getManualFromProductId(productId).lower()
        removeWords = database.getStopwords(productId)
        database.close()

        # Display current product name
        name_label = Label(self, text=name, font=format.SUBHEADER_FONT)
        name_label.grid(row=2, columnspan=3, pady=10, padx=20, sticky=W)

        # Display product category
        category_label = Label(self, text=category, font=format.LABEL_FONT)
        category_label.grid(row=3, columnspan=3, pady=10, padx=20, sticky=W)

        # Display wordcloud graphic of frequently used terms in the product manual
        wc = WordCloud(stopwords=removeWords | STOPWORDS, regexp=r"\w[\w'-]+", collocations=False)
        img = wc.generate(manual).to_image()
        img = ImageTk.PhotoImage(img)
        img_label = Label(self, image=img)
        img_label.image = img
        img_label.grid(row=4, column=0, columnspan=3, pady=10, padx=20, sticky=W)

        # Generate a list of frequent words
        words = wc.process_text(manual)
        frequencies = sorted(words.items(), key=itemgetter(1), reverse=True)

        # Display 50 most frequent terms
        row_n = 5
        column_n = 0
        checked = []
        for i in range(50):
            checked.append(IntVar())
        for i in range(50):
            Checkbutton(
                self,
                text=frequencies[i][0],
                variable=checked[i],
                onvalue=1,
                offvalue=0).grid(
                    row=row_n, column=column_n, padx=10, sticky=W)
            if (column_n <= 1 and row_n == 17) or (column_n > 1 and row_n == 16):
                column_n += 1
                row_n = 5
            else:
                row_n += 1

        # Display button to continue to the next page
        next_step = Button(
            self,
            text="Next",
            bg="Orange",
            width=6,
            font=format.NEXT_FONT,
            fg="White",
            command=lambda: self.getFrame(parent, root, products, index, removeWords, checked, frequencies).tkraise())
        next_step.grid(row=18, column=3, sticky=S)

    # Adds the terms selected by the user to the product's stopwords, adds remaing words to word vector and generates the next page
    def getFrame(self, parent, root, prods, index, stopwords, checked, frequencies):
        # Adding terms to the database
        removeWords, wordvector = self.getRemoved(checked, frequencies)
        totalRemoved = stopwords | removeWords
        database = DataAPI()
        database.updateStopwords(prods[index], totalRemoved)
        database.close()

        # Add remaining terms to the word vectors
        root.wordVectors.append(wordvector)

        # Generate next frame 
        frame = None
        index += 1

        if index == len(prods):
            maxLength = 0
            indices = []

            for i in range(len(prods)):
                if len(root.wordVectors[i]) > maxLength : maxLength = len(root.wordVectors[i])

            for i in range(len(prods)):
                if len(root.wordVectors[i]) < maxLength : indices.append(i)

            if len(indices) == 0:
                frame = ResultPage(parent, root)
            else:
                frame = AddTerms(parent, root, indices, maxLength)
        else:
            frame = DeleteTerms(parent, root, prods, index)

        frame.grid(row=0, column=0, sticky="nsew")
        return frame

    def getRemoved(self, checked, frequencies):
        removeWords = set()
        wordvector = []

        for i in range(50):
            if checked[i].get() == 1:
                removeWords.add(frequencies[i][0])
            else:
                wordvector.append(frequencies[i][0])

        return removeWords, wordvector
