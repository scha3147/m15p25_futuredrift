from Tkinter import *
import Tkinter as tk

from selectProducts import SelectProducts
import format
''' THE SECOND PAGE -- Select the category(s) interested in analysing
    INPUT: The lowest level categories from DB
    OUTPUT: Selected category(s)

    Research materials you might need:
    1. dynamically-add-checkboxes-with-tkinter -- https://stackoverflow.com/questions/13862535/dynamically-add-checkboxes-with-tkinter-in-python-2-7

    Reminder: Change the layout method will cause display errors. The layout method for all widgets in one page should be the same.
    For example, do not use grid() for some widgets and pack() for others.
'''


class SelectCategories(Frame):
    def __init__(self, parent, root):
        Frame.__init__(self, parent)
        label = Label(
            self,
            text="Select the category(s) you are interested in analysing",
            font=format.LABEL_FONT)
        label.grid(row=1, columnspan=3, pady=10, padx=20, sticky=W)
        # TODO
        categories = [
            "Animals & Pet Supplies", "Apparel & Accessories",
            "Arts & Entertainment", "Baby & Toddler", "Business & Industrial",
            "Cameras & Optics", "Electronics", "Food, Beverages & Tobacco",
            "Furniture", "Hardware", "Health & Beauty", "Home & Garden",
            "Luggage & Bags"
            "Mature", "Media", "Office Supplies", "Religious & Ceremonial",
            "Software", "Sporting Goods", "Toys & Games", "Vehicles & Parts"
        ]

        row_n = 3
        checked = []

        for i in range(20):
            checked.append(IntVar())
        for i in range(10):
            Checkbutton(
                self,
                text=categories[i],
                variable=checked[i],
                onvalue=1,
                offvalue=0).grid(
                    row=row_n, column=0, columnspan=2, padx=20, sticky=W)
            row_n += 1
        
        row_n = row_n - 10
        for i in range(10, 20):
            Checkbutton(
                self,
                text=categories[i],
                variable=checked[i],
                onvalue=1,
                offvalue=0).grid(
                    row=row_n, column=2, columnspan=2, padx=20, sticky=W)
            row_n += 1

        next_step = Button(
            self,
            text="Next",
            bg="Orange",
            width=6,
            font=format.NEXT_FONT,
            fg="White",
            command=lambda: self.getFrame(parent, root, self.getSelected(checked, categories)).tkraise())
        next_step.grid(row=15, column=1, columnspan=2, pady=10)

    def getFrame(self, parent, root, selected):
        frame = SelectProducts(parent, root, selected)

        frame.grid(row=0, column=0, sticky="nsew")
        return frame

    def getSelected(self, checked, categories):
        selected = set()

        for i in range(20):
            if checked[i].get() == 1: selected.add(categories[i])

        return selected
