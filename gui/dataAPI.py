# -*- coding: utf-8 -*
from itertools import islice
import psycopg2

#################################
## Author: Wenchang Liu        ##
## Code Reviewer: Edmond Buzby ##
#################################


class DataAPI:
    def __init__(self):
        try:
            # remember to change your own dbname, user, host and password
            self.conn = psycopg2.connect(
                database='COMP3615',
                user='postgres',
                host='localhost',
                password='1')
            print "Connect to database"
        except:
            print "I am unable to connect to the database"
        self.cur = self.conn.cursor()

    # get all products from database order by ascending product names, return a list of name string
    # Example output: ['ASUS Google Nexus 7', 'Alienware M14x R2', ...etc]
    def getAllProducts(self):
        print "Start to get all products"
        query = "SELECT name FROM Product ORDER BY name ASC"
        self.cur.execute(query)
        productList = self.cur.fetchall()
        productList = [i[0] for i in productList]
        print "Return all products"
        # print productList
        return productList

    # getAllProducts(self)

    # get the category name according to input product name, need product name as input and return a name string. Return empty string if not exist
    # Issue 2, Issue 3
    # Example input: iPhone 5
    # Example output: ['Electronics', 'Communications', 'Telephony', 'Mobile phones']
    def getProductCategory(self, productName):
        print "Start to fetch category list"
        query = "SELECT categoryID FROM product where name='" + productName + "'"
        self.cur.execute(query)
        categoryId = self.cur.fetchall()

        if not categoryId:
            print "Category not found"
            return ''

        categoryId = [i[0] for i in categoryId]

        query = "SELECT id,name,parent,level FROM category where id=" + str(
            categoryId[0])
        self.cur.execute(query)
        category = self.cur.fetchall()

        categorys = []
        categorys.insert(0, category[0][1])

        while category[0][2] is not None:
            query = "SELECT id,name,parent,level FROM category where id=" + str(
                category[0][2])
            self.cur.execute(query)
            category = self.cur.fetchall()
            categorys.insert(0, category[0][1])
        print "Return categorys lists"
        # print categorys
        return categorys

    # getProductCategory('iPhone 5')

    # get all categories with related products order by ascending category names, return a list of tuple. The tuples contain category names and relevant product names.
    # Issue 7
    # Example output: [('Electronics', 'Computers', 'Laptops', 'Lenovo IdeaPad Yoga 11'), ('Electronics', 'Communications', 'Telephony', 'Mobile phones', 'iPhone 5'), ...etc]
    def getCategoryAndProduct(self):
        print "Start to get category and product list"
        query = "SELECT name,categoryID FROM product ORDER BY name ASC"
        self.cur.execute(query)
        products = self.cur.fetchall()
        categoryList = []

        for product in products:
            categoryId = product[1]

            categorys = []
            categorys.insert(0, product[0])

            query = "SELECT id,name,parent,level FROM category where id=" + str(
                categoryId)
            self.cur.execute(query)
            category = self.cur.fetchall()

            categorys.insert(0, category[0][1])

            while category[0][2] is not None:
                query = "SELECT id,name,parent,level FROM category where id=" + str(
                    category[0][2])
                self.cur.execute(query)
                category = self.cur.fetchall()
                categorys.insert(0, category[0][1])
            if len(categorys) > 5:
                first = categorys[0]
                second = categorys[1]
                third = categorys[2]
                fourth = categorys[3]
                last = categorys[-1]
                categorys = [first, second, third, fourth, last]
            categorysTuple = tuple(categorys)
            categoryList.insert(0, categorysTuple)

        print "Return category and product list"
        # print categoryList
        return categoryList

    # getCategoryAndProduct()

    # get products as a list according to provided lowest level category, return a string list that contains products name
    # Example input: ('Camera')
    # Example output: ['Nikon D800', 'Samsung NX3000']
    def getProductsByCategoryName(self, categoryName):
        print "Start to get product list by category"
        query = "SELECT id FROM category WHERE name=\'" + categoryName + "\'"
        self.cur.execute(query)
        result = self.cur.fetchall()
        categoryId = [i[0] for i in result]
        if (categoryId is None) or (not categoryId):
            print "The product name is not found"
            return []

        query = "SELECT name FROM product WHERE categoryID=" + str(
            categoryId[0]) + " ORDER BY name DESC"
        self.cur.execute(query)
        products = self.cur.fetchall()
        productList = []
        for product in products:
            productList.insert(0, product[0])
        print "Return products list"
        # print productList
        return productList

    # getProductsByCategory('Video')

    # get category trees with product count, return a list of tuple, tuple contains category tree and relevant product numbers
    # Issue 5
    # Example output: [('Electronics', 'Computers', 'Laptops', '2'), ('Electronics', 'Print, Copy, Scan & Fax', 'Printers, Copiers & Fax Machines', '2'), ...etc]
    def getCategoriesList(self):
        print "Start to get category list"
        query = "SELECT categoryID,COUNT(*) FROM product GROUP BY categoryID ORDER BY categoryID"
        self.cur.execute(query)
        countList = self.cur.fetchall()

        categoryList = []
        for single in countList:
            categoryId = single[0]

            categorys = []
            categorys.insert(0, str(single[1]))
            query = "SELECT id,name,parent,level FROM category where id=" + str(
                categoryId)
            self.cur.execute(query)
            category = self.cur.fetchall()

            categorys.insert(0, category[0][1])

            while category[0][2] is not None:
                query = "SELECT id,name,parent,level FROM category where id=" + str(
                    category[0][2])
                self.cur.execute(query)
                category = self.cur.fetchall()
                categorys.insert(0, category[0][1])

            if len(categorys) > 5:
                first = categorys[0]
                second = categorys[1]
                third = categorys[2]
                fourth = categorys[3]
                last = categorys[-1]
                categorys = [first, second, third, fourth, last]

            categorysTuple = tuple(categorys)
            categoryList.insert(0, categorysTuple)

        query = "SELECT id,name,parent,level FROM category where child=0"
        self.cur.execute(query)
        zeroList = self.cur.fetchall()
        for single in zeroList:
            categorys = []
            categorys.insert(0, 0)
            categorys.insert(0, str(single[1]))
            category = []
            category.insert(0, single)
            while category[0][2] is not None:
                query = "SELECT id,name,parent,level FROM category where id=" + str(
                    category[0][2])
                self.cur.execute(query)
                category = self.cur.fetchall()
                categorys.insert(0, category[0][1])

            if len(categorys) > 5:
                first = categorys[0]
                second = categorys[1]
                third = categorys[2]
                fourth = categorys[3]
                last = categorys[-1]
                categorys = [first, second, third, fourth, last]

            categorysTuple = tuple(categorys)
            categoryList.insert(0, categorysTuple)
        print "Return category list"
        # print categoryList[:10]
        return categoryList

    # getCategoriesList()

    # get category trees with product count, return a list of tuple, tuple contains category tree
    # Issue 4, Issue 6
    # Example output: [('Electronics', 'Computers', 'Laptops'), ('Electronics', 'Print, Copy, Scan & Fax', 'Printers, Copiers & Fax Machines'), ...etc]
    def getPureCategoriesList(self):
        print "Start to get pure category list"
        query = "SELECT categoryID,COUNT(*) FROM product GROUP BY categoryID ORDER BY categoryID"
        self.cur.execute(query)
        countList = self.cur.fetchall()

        categoryList = []
        for single in countList:
            categoryId = single[0]

            categorys = []
            query = "SELECT id,name,parent,level FROM category where id=" + str(
                categoryId)
            self.cur.execute(query)
            category = self.cur.fetchall()

            categorys.insert(0, category[0][1])

            while category[0][2] is not None:
                query = "SELECT id,name,parent,level FROM category where id=" + str(
                    category[0][2])
                self.cur.execute(query)
                category = self.cur.fetchall()
                categorys.insert(0, category[0][1])

            if len(categorys) > 4:
                first = categorys[0]
                second = categorys[1]
                third = categorys[2]
                fourth = categorys[3]
                categorys = [first, second, third, fourth]

            categorysTuple = tuple(categorys)
            categoryList.insert(0, categorysTuple)

        query = "SELECT id,name,parent,level FROM category where child=0"
        self.cur.execute(query)
        zeroList = self.cur.fetchall()
        for single in zeroList:
            categorys = []
            categorys.insert(0, str(single[1]))
            category = []
            category.insert(0, single)
            while category[0][2] is not None:
                query = "SELECT id,name,parent,level FROM category where id=" + str(
                    category[0][2])
                self.cur.execute(query)
                category = self.cur.fetchall()
                categorys.insert(0, category[0][1])

            if len(categorys) > 4:
                first = categorys[0]
                second = categorys[1]
                third = categorys[2]
                fourth = categorys[3]
                categorys = [first, second, third, fourth]

            categorysTuple = tuple(categorys)
            categoryList.insert(0, categorysTuple)
        print "Return pure category list"
        # print categoryList
        return categoryList

    # getPureCategoriesList()

    # get the user manual text for a particular product
    # Issue 9
    # Outputs a string containing the entire text
    def getManualFromProductId(self, productId):
        query = "SELECT description FROM manual WHERE productID = %d;" % productId
        self.cur.execute(query)
        records = self.cur.fetchone()
        manual = str(records[0])

        return manual

    # gets the name of a product given its ID
    # Example output: "iPhone 5"
    def getNameFromProductId(self, productId):
        query = "SELECT name FROM product WHERE id = %d;" % productId
        self.cur.execute(query)
        records = self.cur.fetchone()
        name = str(records[0])

        return name

    # gets the first product ID of a given product name
    def getProductIdFromName(self, productName):
        query = "SELECT id FROM product WHERE name = \'%s\';" % productName
        self.cur.execute(query)
        records = self.cur.fetchone()
        productId = int(records[0])

        return productId

    # gets the set of stopwords associated with a given product
    # Issue 9
    # Example output: {"use", "give", "push"}
    def getStopwords(self, productId):
        query = "SELECT stopwords FROM product WHERE id = %d;" % productId
        self.cur.execute(query)
        records = self.cur.fetchone()
        stopwords = set(str(records[0]).split(','))

        return stopwords

    # updates the set of stopwords of a given product
    # Issue 9
    def updateStopwords(self, productId, removeWords):
        stopwords = ','.join(removeWords)
        query = "UPDATE product SET stopwords = \'%s\' WHERE id = %d; COMMIT;" % (stopwords, productId)
        self.cur.execute(query)

    # closes the connection to the database
    def close(self):
        self.cur.close()
        self.conn.close()
        print "Disconnect with database"
