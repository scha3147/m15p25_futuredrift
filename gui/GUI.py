import Tkinter as tk

from startPage import StartPage
from selectCategories import SelectCategories
from selectProducts import SelectProducts
from selectModels import SelectModels
from deleteTerms import DeleteTerms
from resultPage import ResultPage


class Application(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)

        self.wm_title("Feature Drift Tool")
        self.selected = []
        self.wordVectors = []
        self.upper = None;
        self.lower = None;
        self.ontology = [{"video", "movie"}, {"monitor", "screen", "display"}, 
                         {"image", "photo"}, {"key", "button"}, {"application", "app"}]

        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        frame = StartPage(container, self)
        frame.grid(row=0, column=0, sticky="nsew")
        frame.tkraise()


if __name__ == '__main__':
    app = Application()
    app.mainloop()
