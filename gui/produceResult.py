selectedList = []
wordVectors = []
'''reference product'''
referenceProduct = selectedList[0]
'''models that users would like to drift features from'''
modelList = selectedList[1:]

'''features of reference product'''
feature_reference = wordVectors[0]
model_feature = wordVectors[1:]


def calSimilarity(upper=0.7, lower=0.3):
    related = []
    non_related = []
    similar = []

    for i in range(len(modelList)):
        '''apply algorithm to each model - calculate similarity'''
        B = []

        for j in range(len(feature_reference)):
            if feature_reference[j] in model_feature[i]:
                B.append[1]
            else:
                B.append[0]

        for b in B:
            AB += 1 * b
            BB += b * b

        similarity = AB/(sqrt(len(feature_reference)) * sqrt(BB))
        


        '''similarity between 0.5~0.7, related product
        similarity lower than 0.5, non-related product - interesting product
        similarity higher than 0.7, similar product'''
        if similarity >= lower and similarity <= upper:
            related.append[model]
        elif similarity < lower:
            non_related.append[model]
        elif similarity > upper:
            similar.append[model]

        return related, non_related, similar

