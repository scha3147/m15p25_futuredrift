from Tkinter import *
import Tkinter as tk

from deleteTerms import DeleteTerms
from dataAPI import *
import format
''' THE FOURTH PAGE -- Select a model for each product
    INPUT: Product name from previous page (selectProducts);
           Model name for each product from DB
    OUTPUT: Selected model(s)
'''

##############################
## Author: Li Wang          ##
## Code clean up: Ira Chen
## Code Review: Shengjie Lu ##
##############################


class SelectModels(Frame):
    def __init__(self, parent, root, products):
        Frame.__init__(self, parent)
        label = Label(
            self,
            text="Please select a model for each product",
            font=format.LABEL_FONT)
        label.grid(row=1, columnspan=3, pady=10, padx=20, sticky=W)

        r = 3
        options = []
        database = DataAPI()

        for i in range(len(products)):
            options.append(StringVar(self))
            options[i].set("Please select a model for this product")
            Label(
                self, text=products[i], relief=RIDGE, width=20).grid(
                    row=r, column=0, columnspan=2, padx=20)


            # get models from DB by category name
            model_options = database.getProductsByCategoryName(products[i])

            option = OptionMenu(self, options[i], *model_options)
            option.configure(width=35)
            option.grid(row=r, column=2, columnspan=3)

            r += 1

        database.close()

        # go to next page
        next_step = Button(
            self,
            text="Next",
            bg="Orange",
            width=6,
            font=format.NEXT_FONT,
            fg="White",
            command=lambda: self.getFrame(parent, root, self.getSelected(root, options)))
        next_step.grid(row=15, column=2, pady=10, sticky=S)

    def getFrame(self, parent, root, selected):
        frame = None

        if "Please select a model for this product" in selected:
            tkMessageBox.showinfo(
                title="Reminder",
                message="Please select a model for all products")
        else:
            frame = DeleteTerms(parent, root, selected)

        frame.grid(row=0, column=0, sticky="nsew")
        return frame

    def getSelected(self, root, options):
        database = DataAPI()

        for option in options:
            root.selected.append(database.getProductIdFromName(option.get()))

        database.close()

        return root.selected
