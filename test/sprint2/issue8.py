# -*- coding: utf-8 -*
import sys
sys.path.append('../../gui')
from deleteTerms import *
# from dataAPI import *
import unittest

##########################
## Author: Wenchang Liu ##
##########################

# Issue 8
# For test of issue 8, the function will return the manual of product.


# Sudo:
# manual1 = productid.getManualByProductId(id1)
# manual2 = productid.getManualByProductId(id2)
# manual3 = productid.getManualByProductId(id3)
# manual4 = productid.getManualByProductId(id4)

class testDeleteTerm:
	pass

class TestUM(unittest.TestCase):

	def setUp(self):
		pass

	# test get product 5 name
	def test_getManualByProdId_1(self):
		api = DataAPI()
		self.assertTrue( len(api.getManualFromProdId(5)) > 1 )

	# test get product 15 name
	def test_getManualByProdId_2(self):
		api = DataAPI()
		self.assertTrue( len(api.getManualFromProdId(15)) > 1 )

	# test get product 22 name
	def test_getManualByProdId_3(self):
		api = DataAPI()
		self.assertTrue( len(api.getManualFromProdId(22)) > 1)

if __name__ == '__main__':
	unittest.main()