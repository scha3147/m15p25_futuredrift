# -*- coding: utf-8 -*
import sys
sys.path.append('../../gui')
from selectProducts import *
import unittest

##########################
## Author: Wenchang Liu ##
##########################

# Issue 4
# For test of issue 4, the function will return a list that contains all product type.


# Sudo: count(product type numbers of list).equal(constant number)
# list.contains(specific product 1)
# list.contains(specific product 2)

# test reference: http://pythontesting.net/framework/unittest/unittest-introduction/

class selectProduct:
	pass

class TestUM(unittest.TestCase):

	def setUp(self):
		pass

	# test all products type in interface
	def test_allProduct_list(self):
		api = DataAPI()
		self.assertTrue( len(api.getAllProducts()) > 1)


if __name__ == '__main__':
	unittest.main()