# -*- coding: utf-8 -*
import sys
sys.path.append('../../gui')
from selectProducts import *
import unittest

##########################
## Author: Wenchang Liu ##
##########################

# Issue 6
# For test of issue 6, the function will return a list that may contains up to 5 product types.


# Sudo: count(product type numbers of list).equal(constant number)
# list.contains(specific product 1)
# list.contains(specific product 2)
# list.contains(specific product 3)
# list.contains(specific product 4)
# list.contains(specific product 5)

class selectProductMoreThan5:
	pass

class TestUM(unittest.TestCase):

	def setUp(self):
		pass

	# test all products type in interface no less than 5
	def test_allProduct_list(self):
		api = DataAPI()
		self.assertTrue( len(api.getAllProducts()) > 5)


if __name__ == '__main__':
	unittest.main()