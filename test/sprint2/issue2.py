# -*- coding: utf-8 -*
import sys
sys.path.append('../../gui')
from startPage import *
# from dataAPI import *
import unittest

##########################
## Author: Wenchang Liu ##
##########################

# Issue 2
# For test of issue 2, the selected reference type and model should be returned as a list. The provided list should contains all product type and model

# Sudo: count(elements numbers of list).equal(constant number)
# list.contains(selected products)

# Using getAllProducts()

# test reference: http://pythontesting.net/framework/unittest/unittest-introduction/
class TestUM(unittest.TestCase):

	def setUp(self):
		pass

	# test get list of all products
	def test_allProduct_list(self):
		gui = DataAPI()
		self.assertTrue( len(gui.getAllProducts()) > 1)


if __name__ == '__main__':
	unittest.main()