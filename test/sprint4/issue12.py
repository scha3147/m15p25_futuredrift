# -*- coding: utf-8 -*
import sys
sys.path.append('../../gui')
from deleteTerms import *
import unittest

##########################
## Author: Wenchang Liu ##
##########################

# Issue 12
# For test of issue 12, it returns the manual of the selected products and the product name.

# Sudo:
# list.contains(manual)
# name.equal(getProductNameById())

# test reference: http://pythontesting.net/framework/unittest/unittest-introduction/

class deleteTer:
	pass

class TestUM(unittest.TestCase):

	def setUp(self):
		pass

	# test get manual of product 4 in interface
	def test_manual_4_integrity(self):
		api = DataAPI()
		self.assertTrue( api.getManualFromProductId(4) >= 1)

	# test get manual of product 10 in interface
	def test_manual_10_integrity(self):
		api = DataAPI()
		self.assertTrue( api.getManualFromProductId(10) >= 1)

	# test get product 15 name
	def test_getNameFromProdId_15(self):
		api = DataAPI()
		self.assertEqual(api.getNameFromProductId(15), "Panasonic VIERA VT50A")

	# test get product 22 name
	def test_getNameFromProdId_22(self):
		api = DataAPI()
		self.assertEqual(api.getNameFromProductId(22), "SanDisk Sansa Clip Zip")

if __name__ == '__main__':
	unittest.main()