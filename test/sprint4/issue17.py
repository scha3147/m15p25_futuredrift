# -*- coding: utf-8 -*
import sys
sys.path.append('../../gui')
from selectModels import *
import unittest

##########################
## Author: Wenchang Liu ##
##########################

# Issue 17
# For test of issue 17, it returns the list contains manual of selected product


# Sudo:
# list.contains(getManualFromProductId(1))
# list.contains(getManualFromProductId(2))

# test reference: http://pythontesting.net/framework/unittest/unittest-introduction/

class selectMods:
	pass

class TestUM(unittest.TestCase):

	def setUp(self):
		pass

	# test select model interface
	def test_select_manual_1(self):
		api = DataAPI()
		self.assertTrue( len(api.getManualFromProductId(1)) >= 1)

	# test select model interface
	def test_select_manual_2(self):
		api = DataAPI()
		self.assertTrue( len(api.getManualFromProductId(2)) >= 1)

	# test select model interface
	def test_select_manual_3(self):
		api = DataAPI()
		self.assertTrue( len(api.getManualFromProductId(3)) >= 1)

if __name__ == '__main__':
	unittest.main()