# -*- coding: utf-8 -*
import sys
sys.path.append('../../gui')
from selectCategory import *
import unittest

##########################
## Author: Wenchang Liu ##
##########################

# Issue 14
# For test of issue 14, it returns the name of products and related category


# Sudo:
# getProductCategory1.equals(category1)
# getProductCategory2.equals(category2)

# test reference: http://pythontesting.net/framework/unittest/unittest-introduction/

class selectCate:
	pass

class TestUM(unittest.TestCase):

	def setUp(self):
		pass

	# test get product with correct category name
	def test_getProductByCategoryName_1(self):
		api = DataAPI()
		self.assertEqual(api.getProductsByCategoryName('Camera'), ['Nikon D800', 'Samsung NX3000'])

	# test get product with incorrect category name
	def test_getProductByCategoryName_2(self):
		api = DataAPI()
		self.assertEqual(api.getProductsByCategoryName('Babala'), [])


if __name__ == '__main__':
	unittest.main()