# -*- coding: utf-8 -*
import sys
sys.path.append('../../gui')
from selectProducts import *
import unittest

##########################
## Author: Wenchang Liu ##
##########################

# Issue 15
# For test of issue 4, the function will return the selected product.


# Sudo: count(product type numbers of list).equal(constant number)
# productName1.equals(ProductSelected1)
# productName2.equals(ProductSelected2)

# test reference: http://pythontesting.net/framework/unittest/unittest-introduction/

class selectProduct:
	pass

class TestUM(unittest.TestCase):

	def setUp(self):
		pass

	# test get product 5 name
	def test_getNameFromProdId_5(self):
		api = DataAPI()
		self.assertEqual(api.getNameFromProductId(5), "HTC One X")

	# test get product 15 name
	def test_getNameFromProdId_15(self):
		api = DataAPI()
		self.assertEqual(api.getNameFromProductId(15), "Panasonic VIERA VT50A")

	# test get product 22 name
	def test_getNameFromProdId_22(self):
		api = DataAPI()
		self.assertEqual(api.getNameFromProductId(22), "SanDisk Sansa Clip Zip")


if __name__ == '__main__':
	unittest.main()