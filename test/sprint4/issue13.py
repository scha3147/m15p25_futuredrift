# -*- coding: utf-8 -*
import sys
sys.path.append('../../gui')
from selectProducts import *
import unittest

##########################
## Author: Wenchang Liu ##
##########################

# Issue 13
# For test of issue 13, it returns the products that selected by users

# Sudo:
# select.equals(productName1)
# select.equals(productName1)

# test reference: http://pythontesting.net/framework/unittest/unittest-introduction/

class selProduct:
	pass

class TestUM(unittest.TestCase):

	def setUp(self):
		pass

	# test get product 5 name
	def test_getNameFromProdId_5(self):
		api = DataAPI()
		self.assertEqual(api.getNameFromProductId(5), "HTC One X")

	# test get product 15 name
	def test_getNameFromProdId_15(self):
		api = DataAPI()
		self.assertEqual(api.getNameFromProductId(15), "Panasonic VIERA VT50A")

	# test get product 22 name
	def test_getNameFromProdId_22(self):
		api = DataAPI()
		self.assertEqual(api.getNameFromProductId(22), "SanDisk Sansa Clip Zip")


if __name__ == '__main__':
	unittest.main()