# -*- coding: utf-8 -*
import sys
sys.path.append('../../gui')
from selectModels import *
import unittest

##########################
## Author: Wenchang Liu ##
##########################

# Issue 16
# For test of issue 16, it returns the products by category name


# Sudo:
# productName1 = getProduct(categoryName1)
# productName2 = getProduct(categoryName2)
# productName3 = getProduct(categoryName3)
# productName4 = getProduct(categoryName4)

# test reference: http://pythontesting.net/framework/unittest/unittest-introduction/

class selectModel:
	pass

class TestUM(unittest.TestCase):

	def setUp(self):
		pass

	# test get product with correct category name
	def test_getProductByCategoryName_1(self):
		api = DataAPI()
		self.assertEqual(api.getProductsByCategoryName('Camera'), ['Nikon D800', 'Samsung NX3000'])

	# test get product with incorrect category name
	def test_getProductByCategoryName_2(self):
		api = DataAPI()
		self.assertEqual(api.getProductsByCategoryName('Babala'), [])

if __name__ == '__main__':
	unittest.main()