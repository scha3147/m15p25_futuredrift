# -*- coding: utf-8 -*
import sys
sys.path.append('../../gui')
from dataAPI import *
import unittest

##########################
## Author: Wenchang Liu ##
##########################

# test reference: http://pythontesting.net/framework/unittest/unittest-introduction/

class TestUM(unittest.TestCase):

	def setUp(self):
		pass

	# test mutiple product name
	def test_categoryList_single_name_1(self):
		api = DataAPI()
		self.assertEqual( api.getProductCategory('HTC One X'), ['Electronics', 'Communications', 'Telephony', 'Mobile phones'])

	def test_categoryList_single_name_2(self):
		api = DataAPI()
		self.assertEqual( api.getProductCategory('Iiyama XB2472HD'), ['Electronics', 'Video', 'Computer Monitors'])

	def test_categoryList_single_name_3(self):
		api = DataAPI()
		self.assertEqual( api.getProductCategory('Nintendo 3DS'), ['Electronics', 'Video Game Consoles'])

	def test_categoryList_single_name_4(self):
		api = DataAPI()
		self.assertEqual( api.getProductCategory('Alienware M14x R2'), ['Electronics', 'Computers', 'Laptops'])

	def test_categoryList_single_name_5(self):
		api = DataAPI()
		self.assertEqual( api.getProductCategory('Samsung Google Nexus 10'), ['Electronics', 'Computers', 'Tablet Computers'])

	# test not exist product name
	def test_categoryName_invalid_name_1(self):
		api = DataAPI()
		self.assertEqual( api.getProductCategory(' Nintendo 3DS '), '')

	def test_categoryName_invalid_name_2(self):
		api = DataAPI()
		self.assertEqual( api.getProductCategory('iPhone 4S'), '')

	def test_categoryName_invalid_name_3(self):
		api = DataAPI()
		self.assertEqual( api.getProductCategory('HUAWEI S9'), '')

	def test_categoryName_invalid_name_4(self):
		api = DataAPI()
		self.assertEqual( api.getProductCategory('asdlfjl;i324io89/.dadf'), '')


	# test get list of all products
	def test_allProduct_list(self):
		api = DataAPI()
		self.assertTrue( len(api.getAllProducts()) > 1)

	# test number of pure categories
	def test_pureCategories_list(self):
		api = DataAPI()
		self.assertTrue( len(api.getPureCategoriesList()) > 1)

	def test_allCategories_list(self):
		api = DataAPI()
		self.assertTrue( len(api.getCategoriesList()) > 1)

	# test list of categories and relevant products
	def test_categoryAndProduct_list(self):
		api = DataAPI()
		self.assertTrue( len(api.getCategoryAndProduct()) > 1)

	# test get list of product by category name
	def test_getProductByCategoryName_list(self):
		api = DataAPI()
		self.assertEqual(api.getProductsByCategory('Camera'), ['Nikon D800', 'Samsung NX3000'])

	# test get empty list by category name
	def test_getProductByCategoryName_empty(self):
		api = DataAPI()
		self.assertEqual( api.getProductsByCategoryName('adadf'), [])

if __name__ == '__main__':
	unittest.main()
