# -*- coding: utf-8 -*
import sys
sys.path.append('../../gui')
from startPage import *
import unittest

##########################
## Author: Wenchang Liu ##
##########################

# Issue 19
# For test of issue 19, the function will return the correct selection that chosen by users.


# Sudo:
# selectionName1.equals(chosenName1)
# selectionName2.equals(chosenName2)
# selectionName3.equals(chosenName3)

# test reference: http://pythontesting.net/framework/unittest/unittest-introduction/

class start:
	pass

class TestUM(unittest.TestCase):

	def setUp(self):
		pass

	# test get the all products that provide selections
	def test_getProductList(self):
		api = DataAPI()
		self.assertTrue( api.getAllProducts() > 0 )


if __name__ == '__main__':
	unittest.main()