# -*- coding: utf-8 -*
import sys
sys.path.append('../../gui')
# from produceResult import *
from dataAPI import *
import unittest

##########################
## Author: Wenchang Liu ##
##########################

# Issue 20
# For test of issue 20, it returns the correct products that after calculate the similarity

# Sudo:
# productName1 = getProduct(productId1)
# productName2 = getProduct(productId2)

# test reference: http://pythontesting.net/framework/unittest/unittest-introduction/

class calSimilar:
	pass

class TestUM(unittest.TestCase):

	def setUp(self):
		pass

	# test get product with correct product id after calculation
	def test_getCalculatedResult_1(self):
		api = DataAPI()
		self.assertEqual( api.getNameFromProductId(1), 'iPhone 5')

	# test get product with correct product id after calculation
	def test_getCalculatedResult_2(self):
		api = DataAPI()
		self.assertEqual( api.getNameFromProductId(2), 'Samsung Galaxy S III')

if __name__ == '__main__':
	unittest.main()