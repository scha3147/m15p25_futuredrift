# -*- coding: utf-8 -*
import sys
sys.path.append('../../gui')
from dataAPI import *
import unittest

##########################
## Author: Wenchang Liu ##
##########################

# Issue 21
# For test of issue 21, it returns the similarity results


# Sudo:
# isTrue(result.equals(productName1))
# isTrue(result.equals(productName2))

# test reference: http://pythontesting.net/framework/unittest/unittest-introduction/

class result:
	pass

class TestUM(unittest.TestCase):

	def setUp(self):
		pass

	# test get first result
	def test_getResult_1(self):
		api = DataAPI()
		self.assertTrue( len(api.getProductsByCategoryName('Camera')) is not None)

if __name__ == '__main__':
	unittest.main()