# -*- coding: utf-8 -*
import sys
sys.path.append('../../gui')
from addTerms import *
import unittest

##########################
## Author: Wenchang Liu ##
##########################

# Issue 18
# For test of issue 18, it returns the different stopwords that users chosen


# Sudo:
# db.getStopwords(word1).equals(word1)
# db.getStopwords(word2).equals(word2)

# test reference: http://pythontesting.net/framework/unittest/unittest-introduction/

class addTerm:
	pass

class TestUM(unittest.TestCase):

	def setUp(self):
		pass

	# test to get stored stop words according to related product id
	def test_getStopwords_1(self):
		api = DataAPI()
		self.assertFalse(api.getStopwords(1) is None)

	# test to get stored stop words according to related product id
	def test_getStopwords_2(self):
		api = DataAPI()
		self.assertTrue(api.getStopwords(2) is not None)


if __name__ == '__main__':
	unittest.main()