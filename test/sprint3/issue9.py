# -*- coding: utf-8 -*
import sys
sys.path.append('../../gui')
from deleteTerms import *
# from dataAPI import *
import unittest

##########################
## Author: Wenchang Liu ##
##########################

# Issue 9
# For test of issue 9, the function will return a list that contains terms.


# Sudo: count(product type numbers of list).equal(constant number)
# list.contains(term 1)
# list.contains(term 2)
# list.contains(term 3)
# list.contains(term 4)
# list.contains(term 5)

class testDeleteTerm:
	pass

class TestUM(unittest.TestCase):

	def setUp(self):
		pass

	# test get product 5 name
	def test_getNameFromProdId_5(self):
		api = DataAPI()
		self.assertEqual(api.getNameFromProdId(5), "HTC One X")

	# test get product 15 name
	def test_getNameFromProdId_15(self):
		api = DataAPI()
		self.assertEqual(api.getNameFromProdId(15), "Panasonic VIERA VT50A")

	# test get product 22 name
	def test_getNameFromProdId_22(self):
		api = DataAPI()
		self.assertEqual(api.getNameFromProdId(22), "SanDisk Sansa Clip Zip")

if __name__ == '__main__':
	unittest.main()