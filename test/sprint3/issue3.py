
# -*- coding: utf-8 -*
import sys
sys.path.append('../../gui')
from startPage import *
import unittest

##########################
## Author: Wenchang Liu ##
##########################

# Issue 3
# For test of issue 3, it returns the product category list from the selected product

# Sudo:
# list.contains(related categories)
# list.equal(getProductCategory)

# test reference: http://pythontesting.net/framework/unittest/unittest-introduction/

class startPage:
	pass

class TestUM(unittest.TestCase):

	def setUp(self):
		pass

	# test start page interface
	def test_startPage_data_integrity(self):
		api = DataAPI()
		self.assertEqual( api.getProductCategory('iPhone 5'), ['Electronics', 'Communications', 'Telephony', 'Mobile phones'])


if __name__ == '__main__':
	unittest.main()