# -*- coding: utf-8 -*
import sys
sys.path.append('../../gui')
from selectCategory import *
import unittest

##########################
## Author: Wenchang Liu ##
##########################

# Issue 5
# For test of issue 5, it returns the list contains number of products on related category type


# Sudo:
# list.equals(['A', 'B', 'C', 0]])
# list.equals(['D', 'E', 'F', 'G', 12]])

# test reference: http://pythontesting.net/framework/unittest/unittest-introduction/

class selectCate:
	pass

class TestUM(unittest.TestCase):

	def setUp(self):
		pass

	# test start page interface
	def test_selectCategory_1(self):
		api = DataAPI()
		self.assertTrue( len(api.getCategoriesList()) > 1)


if __name__ == '__main__':
	unittest.main()