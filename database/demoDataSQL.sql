﻿select * from product;
select * from category;
select * from product_property;
select * from manual;

SELECT *
FROM information_schema.columns
WHERE table_name = 'product';

SELECT *
FROM information_schema.columns
WHERE table_name = 'category';

SELECT *
FROM information_schema.columns
WHERE table_name = 'product_property';

SELECT *
FROM information_schema.columns
WHERE table_name = 'manual';

EXPLAIN SELECT product_property.*, product.name,category.name FROM product_property LEFT JOIN product ON product.id=product_property.productID INNER JOIN category ON product.categoryID=category.id;

