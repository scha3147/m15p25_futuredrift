CREATE SCHEMA feature_drift;

CREATE TABLE Category (
  id                SERIAL PRIMARY KEY,
  name              VARCHAR(255) NOT NULL,
  description       TEXT
  
);

CREATE TABLE Product (
  id                    SERIAL PRIMARY KEY,
  name                  VARCHAR(255) NOT NULL,
  status                INTEGER NOT NULL,
  categoryID            INTEGER REFERENCES Category(id)

);

CREATE TABLE Product_property (
  id                    SERIAL PRIMARY KEY,
  component             VARCHAR(255),
  component_model       VARCHAR(255),
  detail                TEXT,
  productID            INTEGER REFERENCES Product(id)

);


CREATE TABLE Manual (
  id                    SERIAL PRIMARY KEY,
  URL                   VARCHAR(400),
  description           TEXT,
  productID            INTEGER REFERENCES Product(id)

);
