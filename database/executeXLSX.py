# -*- coding: utf-8 -*
from openpyxl import load_workbook
from django.utils.encoding import smart_str, smart_unicode
from openpyxl.utils import coordinate_from_string, column_index_from_string
from itertools import islice
import psycopg2
import os
import string

##########################
## Author: Wenchang Liu ##
##########################

# create database called COMP3615, username=postgres, password=1

try:
	# rememeber to change your own dbname, user, host and password
    # conn = psycopg2.connect("dbname='COMP3615' user='postgres' host='localhost'")
    conn = psycopg2.connect("dbname='COMP3615' user='postgres' host='localhost' password='1'")
    print "Connect to database"
except:
    print "I am unable to connect to the database"

cur = conn.cursor()
print "Start to read from xlsx"

fn = os.path.join(os.path.dirname(__file__), 'all products.xlsx')

#need to substitute with your own xlsx file path
workbook = load_workbook(fn, data_only=True)

query = "DROP SCHEMA IF EXISTS feature_drift CASCADE; CREATE SCHEMA feature_drift; CREATE TABLE Category (id SERIAL PRIMARY KEY, name VARCHAR(255) NOT NULL, description TEXT); CREATE TABLE Product (id SERIAL PRIMARY KEY, name VARCHAR(255) NOT NULL, status INTEGER NOT NULL, categoryID INTEGER REFERENCES Category(id)); CREATE TABLE Product_property (id SERIAL PRIMARY KEY, component VARCHAR(255), component_model VARCHAR(255), detail TEXT, productID INTEGER REFERENCES Product(id)); CREATE TABLE Manual ( id SERIAL PRIMARY KEY, URL VARCHAR(400), description TEXT, productID INTEGER REFERENCES Product(id));"
cur.execute(query)
conn.commit()

print "Start to insert data"
for sheet in workbook.worksheets:
	# To skip the hidden sheets
	if sheet.sheet_state == 'hidden':
		continue
	# print sheet.title
	query = "INSERT INTO Category(name) VALUES (\'" + sheet.title.strip() + "\');"
	cur.execute(query)
	conn.commit()
	query = "SELECT id FROM Category WHERE name=\'" + sheet.title.strip() + "\';"
	cur.execute(query)
	temp = cur.fetchall()
	conn.commit()
	categoryID = [i[0] for i in temp]
	products = []
	for row in islice(sheet.iter_rows(), 1):
		# print row
		for cell in row[2:]:
			query = "INSERT INTO Product(name, status, categoryID) VALUES (\'" + smart_str(cell.value).strip() + "\', 0, " + str(categoryID[0]) + ");"
			cur.execute(query)
			conn.commit()
			query = "SELECT id FROM Product WHERE name=\'" + smart_str(cell.value).strip() + "\';"
			cur.execute(query)
			temp = cur.fetchall()
			conn.commit()
			productID = [i[0] for i in temp]
			products.append(str(productID[0]))
			# print smart_str(cell.value)
	index = 0
	for row in sheet.iter_rows(row_offset=1):
		# print row
		for cell in row[2:]:
			if row[0].value:
				query = "INSERT INTO Product_property(component, component_model, detail, productID) VALUES (\'" + smart_str(row[0].value).strip() + "\', \'" + smart_str(row[1].value).strip() + "\', \'" + smart_str(cell.value).strip() + "\', " + products[index] + ");"
				cur.execute(query)
				conn.commit()
				# print smart_str(row[0].value)
				# print smart_str(row[1].value)
				# print smart_str(cell.value),
				# print products[index]
				index = index + 1
		index = 0

fn = os.path.join(os.path.dirname(__file__), 'manual.sql')
cur.execute(open(fn, "r").read())
conn.commit()

query = "ALTER TABLE Category ADD COLUMN parent INTEGER NULL DEFAULT NULL;"
cur.execute(query)
conn.commit()

query = "ALTER TABLE Category ADD COLUMN level INTEGER NULL DEFAULT NULL;"
cur.execute(query)
conn.commit()

query = "ALTER TABLE Category ADD COLUMN child INTEGER NULL DEFAULT NULL;"
cur.execute(query)
conn.commit()

query = "ALTER TABLE product ADD COLUMN stopwords TEXT NULL DEFAULT NULL;"
cur.execute(query)
conn.commit()

fn = os.path.join(os.path.dirname(__file__), 'updated_category_table.sql')
cur.execute(open(fn, "r").read())
conn.commit()





#UPDATE the data base to the whole taxonomy
fn = os.path.join(os.path.dirname(__file__), 'product taxonomy.xlsx')
workbook = load_workbook(fn, data_only=True)

for sheet in workbook.worksheets:
	counter = 0
	for row in sheet.iter_rows():
		if counter == 0:
			counter = 1
			continue
		pre = None
		for cell in row:
			if cell.value:
				#print (cell.value)
				name = string.replace(smart_str(cell.value).strip(), '\'', '\'\'')
				#print (name)
				query = "SELECT id FROM Category WHERE name=\'" + name + "\';"
				cur.execute(query)
				temp = cur.fetchone()
				if temp is None:
					if column_index_from_string(cell.column) == 1:
						query = "INSERT INTO Category(name, level) VALUES (\'" + name + "\', 0);"
						cur.execute(query)
						conn.commit()
					else:
						query = "INSERT INTO Category(name, parent, level, child) VALUES (\'" + name + "\', " + str(pre[0]) + ", " + str(column_index_from_string(cell.column)-1) + ", 0);"
						cur.execute(query)
						conn.commit()
					query = "SELECT id FROM Category WHERE name=\'" + name + "\';"
					cur.execute(query)
					pre = cur.fetchone()
				else:
					pre = temp


conn.close()
print "Successfull insert data"


