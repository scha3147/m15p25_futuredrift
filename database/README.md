# Setup Instructions #

Following are instructions to set up the database and run the word cloud generator, suitable for a Linux environment e.g. the School of IT machines. Instructions are assuming your working directory is “m15p25_futuredrift/database/”.

## Database Setup ##

Ensure PostgreSQL is installed. Then, run the following commands in the terminal:

    createuser m15p25 --createdb
    createdb COMP3615 -U m15p25
    psql COMP3615 -U m15p25

You should now be in the PostgreSQL command line, with the prompt

    COMP3615=>

Next, run the following commands in PostgreSQL:

    \i database_version1.sql

   This will create the schema and tables for the database. After you run this command there should be a single “CREATE SCHEMA” line and four “CREATE TABLE” lines printed to the screen.

    \i Database_data_P1.sql

   This will insert the product and category data into their associated tables. After running this, there should be a number of lines “INSERT 1 0” printed to the screen.

    \i manual.sql

   This will insert all the user manuals into the database. Again, there should be a number of “INSERT 1 0” lines printed to the screen after running this command.

After this has been successfully completed, the database should now be completely set up and ready to use. You can exit the PostgreSQL command line either by running the “\q” command or pressing Ctrl+D.

## Python Setup ##

The following Python modules will need to be installed to the machine:

* psycopg2 - This is for connecting to and reading from the database
* wordcloud - This is for generating the actual word cloud graphic
* matplotlib - This is for displaying the graphic to the screen

Some of these could already be installed, however to make sure, run the following commands in the terminal:

    pip install pyscopg2
    pip install wordcloud
    pip install matplotlib

Once all of these have been installed, you are now ready to run the word cloud generating script “cloud.py” by running the following command in the terminal:

    python cloud.py

The script should successfully connect to the database you have just made, and will ask you for some input. The program will search the database for a product that has a name containing your input as a substring, and then generate a word cloud from the user manual of the first matching result. To test, input “iPhone” and a word cloud should be generated from the iPhone 5 user manual, which should contain many of the words commonly associated with the iPhone 5, including the word “iPhone”.

This script is a proof-of-concept for the back-end of our system which will have to generate word clouds based on the user’s input.