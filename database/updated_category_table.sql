INSERT INTO Category(name,level) VALUES ('Electronics', 0);           --11
INSERT INTO Category(name, parent, level) VALUES ('Communications', 11, 1);      --12
INSERT INTO Category(name, parent, level) VALUES ('Telephony', 12, 2);           --13
INSERT INTO Category(name, level) VALUES ('Cameras & Optics', 0);      --14
INSERT INTO Category(name, parent, level) VALUES ('Video', 11, 1);                 --15
INSERT INTO Category(name, parent, level) VALUES ('Computers', 11, 1);           --16
INSERT INTO Category(name, parent, level) VALUES ('Audio', 11, 1);               --17
INSERT INTO Category(name, parent, level) VALUES ('Audio Players & Recorders', 17, 2); --18
INSERT INTO Category(name, parent, level) VALUES ('Computers', 11, 1);		 --19
INSERT INTO Category(name, parent, level) VALUES ('Print, Copy, Scan & Fax', 11, 1); --20



UPDATE Category
SET name = 'Mobile phones', parent = 13, level = 3
Where id = 1;

UPDATE Category
SET name = 'Camera', parent = 14, level = 1
Where id = 2;

UPDATE Category
SET name = 'Computer Monitors', parent = 15, level = 2
Where id = 3;

UPDATE Category
SET name = 'Televisions', parent = 15, level = 2
Where id = 4;

UPDATE Category
SET name = 'Tablet Computers', parent = 16, level = 2
Where id = 5;

UPDATE Category
SET name = 'MP3 Players', parent = 18, level = 3
Where id = 6;

UPDATE Category
SET name = 'Video Game Consoles', parent = 11, level = 1
Where id = 7;

UPDATE Category
SET name = 'Desktop Computers', parent = 16, level = 2
Where id = 8;

UPDATE Category
SET name = 'Printers, Copiers & Fax Machines', parent = 20, level = 2
Where id = 9;

UPDATE Category
SET name = 'Laptops', parent = 16, level = 2
Where id = 10;

