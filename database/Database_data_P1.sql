﻿/**************************/
/***** Category ***********/
/**************************/


INSERT INTO Category(name) VALUES ('Mobile');
INSERT INTO Category(name) VALUES ('Cinema');
INSERT INTO Category(name) VALUES ('Monitor');
INSERT INTO Category(name) VALUES ('TV');
INSERT INTO Category(name) VALUES ('Tablet');
INSERT INTO Category(name) VALUES ('MP3 Player');
INSERT INTO Category(name) VALUES ('Video Game console');
INSERT INTO Category(name) VALUES ('Desktop PC');
INSERT INTO Category(name) VALUES ('Printer');
INSERT INTO Category(name) VALUES ('Laptop1');


/**************************/
/***** Product ************/
/**************************/


INSERT INTO Product(name, status, categoryID) VALUES ('iPhone 5', 0, 1);
INSERT INTO Product(name, status, categoryID) VALUES ('Samsung Galaxy S III', 0, 1);
INSERT INTO Product(name, status, categoryID) VALUES ('Nexus 4', 0, 1);
INSERT INTO Product(name, status, categoryID) VALUES ('Samsung Galaxy Note 2', 0, 1);
INSERT INTO Product(name, status, categoryID) VALUES ('HTC One X', 0, 1);


INSERT INTO Product(name, status, categoryID) VALUES ('Nikon D800', 0, 2);
INSERT INTO Product(name, status, categoryID) VALUES ('Samsung NX3000', 0, 2);


INSERT INTO Product(name, status, categoryID) VALUES ('Philips 273E3LHSB/00', 0, 3);
INSERT INTO Product(name, status, categoryID) VALUES ('ViewSonic VP2365-LED', 0, 3);
INSERT INTO Product(name, status, categoryID) VALUES ('NEC MultiSync EX231W', 0, 3);
INSERT INTO Product(name, status, categoryID) VALUES ('Iiyama XB2472HD', 0, 3);


INSERT INTO Product(name, status, categoryID) VALUES ('Panasonic VIERA ST50A', 0, 4);
INSERT INTO Product(name, status, categoryID) VALUES ('Samsung Series 8', 0, 4);
INSERT INTO Product(name, status, categoryID) VALUES ('Sony BRAVIA HX850', 0, 4);
INSERT INTO Product(name, status, categoryID) VALUES ('IPanasonic VIERA VT50A', 0, 4);


INSERT INTO Product(name, status, categoryID) VALUES ('ASUS Google Nexus 7', 0, 5);
INSERT INTO Product(name, status, categoryID) VALUES ('Samsung Google Nexus 10', 0, 5);
INSERT INTO Product(name, status, categoryID) VALUES ('Amazon Kindle Fire HD', 0, 5);
INSERT INTO Product(name, status, categoryID) VALUES ('Asus Transformer Pad Infinity 700 LTE', 0, 5);
INSERT INTO Product(name, status, categoryID) VALUES ('Samsung Galaxy Note 10.1', 0, 5);
INSERT INTO Product(name, status, categoryID) VALUES ('Microsoft Surface RT', 0, 5);


INSERT INTO Product(name, status, categoryID) VALUES ('SanDisk Sansa Clip Zip', 0, 6);
INSERT INTO Product(name, status, categoryID) VALUES ('iPod nano (7th Gen/2.5" Multitouch)', 0, 6);
INSERT INTO Product(name, status, categoryID) VALUES ('Samsung Galaxy Player 3.6', 0, 6);


INSERT INTO Product(name, status, categoryID) VALUES ('Sony Playstation 3 Slim', 0, 7);
INSERT INTO Product(name, status, categoryID) VALUES ('Microsoft Xbox 360 Slim', 0, 7);
INSERT INTO Product(name, status, categoryID) VALUES ('Nintendo Wii Mario Kart Bundle', 0, 7);
INSERT INTO Product(name, status, categoryID) VALUES ('Nintendo 3DS', 0, 7);


INSERT INTO Product(name, status, categoryID) VALUES ('Gateway SX2850-33', 0, 8);
INSERT INTO Product(name, status, categoryID) VALUES ('Velocity Micro Edge Z40', 0, 8);
INSERT INTO Product(name, status, categoryID) VALUES ('Digital Storm ODE Level 3', 0, 8);
INSERT INTO Product(name, status, categoryID) VALUES ('HP TouchSmart 610q', 0, 8);
INSERT INTO Product(name, status, categoryID) VALUES ('Apple iMac "Core i7" 3.4 27-Inch', 0, 8);


INSERT INTO Product(name, status, categoryID) VALUES ('HP Officejet Pro X576dw Multifunction', 0, 9);
INSERT INTO Product(name, status, categoryID) VALUES ('Dell B1163w Mono Laser Multifunction', 0, 9);


INSERT INTO Product(name, status, categoryID) VALUES ('Alienware M14x R2', 0, 10);
INSERT INTO Product(name, status, categoryID) VALUES ('Lenovo IdeaPad Yoga 11', 0, 10);