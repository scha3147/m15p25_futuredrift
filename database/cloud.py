import psycopg2
import sys
import pprint
from wordcloud import WordCloud 

# Change these connection settings as required
conn_string = "host='localhost' dbname='COMP3615' user='m15p25'"

print("Connecting to database\n ->%s" % conn_string)
conn = psycopg2.connect(conn_string)
cursor = conn.cursor()
search = input("Please enter your search query: ")
cursor.execute("SELECT description FROM manual m JOIN product p ON (p.id = m.ProductId) WHERE name LIKE '%%%s%%'" % search)
records = cursor.fetchone()
manual = str(records[0])
wordcloud = WordCloud(width=853, height=480).generate(manual)
import matplotlib.pyplot as plt
plt.imshow(wordcloud, interpolation='bilinear')
plt.axis("off")
plt.show()