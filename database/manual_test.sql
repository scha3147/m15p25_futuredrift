########################################
###            Manual Test           ###
###           by Shengjie Lu         ###
########################################


## get all tuples from every table of the database
SELECT * FROM Category;

SELECT * FROM Product;

SELECT * FROM Product_property;

SELECT * FROM Manual;



## get all numbers of tuples stored in the each table of database
SELECT COUNT(*) FROM Category;

SELECT COUNT(*) FROM Product;

SELECT COUNT(*) FROM Product_property;

SELECT COUNT(*) FROM Manual;



## test random product to get their category name
SELECT Category.name FROM Category,Product WHERE Product.name = 'Nikon D800' and Product.categoryID=Category.id;

SELECT Category.name FROM Category,Product WHERE Product.name = 'Samsung NX3000' and Product.categoryID=Category.id;

SELECT Category.name FROM Category,Product WHERE Product.name = 'iPhone 5' and Product.categoryID=Category.id;

SELECT Category.name FROM Category,Product WHERE Product.name = 'Samsung Galaxy S III' and Product.categoryID=Category.id;

SELECT Category.name FROM Category,Product WHERE Product.name = 'Nexus 4' and Product.categoryID=Category.id;

SELECT Category.name FROM Category,Product WHERE Product.name = 'Samsung Galaxy Note 2' and Product.categoryID=Category.id;

SELECT Category.name FROM Category,Product WHERE Product.name = 'HTC One X' and Product.categoryID=Category.id;

SELECT Category.name FROM Category,Product WHERE Product.name = 'ASUS Google Nexus 7' and Product.categoryID=Category.id;

SELECT Category.name FROM Category,Product WHERE Product.name = 'Samsung Google Nexus 10' and Product.categoryID=Category.id;

SELECT Category.name FROM Category,Product WHERE Product.name = 'Amazon Kindle Fire HD' and Product.categoryID=Category.id;

SELECT Category.name FROM Category,Product WHERE Product.name = 'Asus Transformer Pad Infinity 700 LTE' and Product.categoryID=Category.id;

SELECT Category.name FROM Category,Product WHERE Product.name = 'Asus Transformer Pad Infinity 700 LTE' and Product.categoryID=Category.id;

SELECT Category.name FROM Category,Product WHERE Product.name = 'Microsoft Surface RT' and Product.categoryID=Category.id;

SELECT Category.name FROM Category,Product WHERE Product.name = 'Sony Playstation 3 Slim' and Product.categoryID=Category.id;




## get all the products given by a category
SELECT Product.name From Category,Product WHERE Category.name = 'Mobile' AND Category.id = product.categoryID;

SELECT Product.name From Category,Product WHERE Category.name = 'Camera' AND Category.id = product.categoryID;

SELECT Product.name From Category,Product WHERE Category.name = 'Monitors' AND Category.id = product.categoryID;

SELECT Product.name From Category,Product WHERE Category.name = 'TV' AND Category.id = product.categoryID;

SELECT Product.name From Category,Product WHERE Category.name = 'Tablet' AND Category.id = product.categoryID;

SELECT Product.name From Category,Product WHERE Category.name = 'Mp3 Player' AND Category.id = product.categoryID;

SELECT Product.name From Category,Product WHERE Category.name = 'Video Game console' AND Category.id = product.categoryID;

SELECT Product.name From Category,Product WHERE Category.name = 'Desktop PC' AND Category.id = product.categoryID;

SELECT Product.name From Category,Product WHERE Category.name = 'Printer' AND Category.id = product.categoryID;

SELECT Product.name From Category,Product WHERE Category.name = 'Laptop1' AND Category.id = product.categoryID;



## get the manual given by random product name
SELECT Manual.description FROM Manual,Product WHERE Product.name = 'Sony Playstation 3 Slim' and Product.id=Manual.productID;

SELECT Manual.description FROM Manual,Product WHERE Product.name = 'Samsung NX3000' and Product.id=Manual.productID;

SELECT Manual.description FROM Manual,Product WHERE Product.name = 'Alienware M14x R2' and Product.id=Manual.productID;

SELECT Manual.description FROM Manual,Product WHERE Product.name = 'Dell B1163w Mono Laser Multifunction' and Product.id=Manual.productID;

SELECT Manual.description FROM Manual,Product WHERE Product.name = 'HP Officejet Pro X576dw Multifunction' and Product.id=Manual.productID;

SELECT Manual.description FROM Manual,Product WHERE Product.name = 'Panasonic VIERA ST50A' and Product.id=Manual.productID;

SELECT Manual.description FROM Manual,Product WHERE Product.name = 'Samsung Series 8' and Product.id=Manual.productID;

SELECT Manual.description FROM Manual,Product WHERE Product.name = 'Sony BRAVIA HX850' and Product.id=Manual.productID;

SELECT Manual.description FROM Manual,Product WHERE Product.name = 'Panasonic VIERA VT50A' and Product.id=Manual.productID;

SELECT Manual.description FROM Manual,Product WHERE Product.name = 'Philips 273E3LHSB/00' and Product.id=Manual.productID;

SELECT Manual.description FROM Manual,Product WHERE Product.name = 'ViewSonic VP2365-LED' and Product.id=Manual.productID;

SELECT Manual.description FROM Manual,Product WHERE Product.name = 'NEC MultiSync EX231W' and Product.id=Manual.productID;

